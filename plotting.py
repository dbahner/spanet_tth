import os
from argparse import ArgumentParser
import numpy as np
import glob
from subprocess import run

import PyATLASstyle as atlas
import matplotlib as mtp
import matplotlib.pyplot as plt
from tensorflow.python.summary.summary_iterator import summary_iterator


def main(input_file: str,
         output_folder: str,
         log_folder: str):
    if input_file == "":
        input_file = log_folder + "/predicted_jets.npy"
    if output_folder == "":
        output_folder = log_folder + "/plots/"
    
    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)
    dic = np.load(input_file, allow_pickle=True)[()]
    variables = {"mass": r"$M_{}$ [GeV]", "pt": r"$p_{}$ [GeV]", "e": r"$E_{}$ [GeV]", "phi": r"$\phi_{}$ [rad]", "eta": r"$\eta_{}$", "score": r"SPANet output for ${}$"}
    rename = {"th": "t\mathrm{, had}", "tl": "t\mathrm{, lep}", "H": "H"}

    atlas.applyATLASstyle(mtp)

    WP = input_file.split("_")[-1].split(".")[0]
    created_pdfs = []
    for res_par in dic:
        for var in variables:
            back = dic[res_par][var][~dic[res_par]["matchable"]]
            w_back = dic[res_par]["weight"][~dic[res_par]["matchable"]]
            sig = dic[res_par][var][dic[res_par]["matchable"] & ~dic[res_par]["matched"]]
            w_sig = dic[res_par]["weight"][dic[res_par]["matchable"] & ~dic[res_par]["matched"]]
            matched = dic[res_par][var][dic[res_par]["matched"]]
            w_matched = dic[res_par]["weight"][dic[res_par]["matched"]]
            if res_par == "H" and var == "mass":
                print(len(matched))
                print(len(sig))
                print(len(back))
            plot_var_distro(variables[var].format(r"{" + f"{rename[res_par]}" + r"}"), output_folder + f"/{res_par}_{var}_{WP}.pdf", [matched, sig, back], [w_matched, w_sig, w_back], 100)
            fig, ax = plt.subplots()
            if var == "mass":
                #hist, _, _ = ax.hist([matched, sig, back], bins = 100, range=(0, 300), weights=[1/(len(matched) + len(sig)) * np.ones_like(matched), 1/(len(matched) + len(sig)) * np.ones_like(sig), 1/len(back) * np.ones_like(back)], histtype='step', label=["matched", "wrongly matched", "unmatchable"])
                hist, _, _ = ax.hist([matched, sig, back], bins = 100, range=(0, 300), weights=[1/len(matched) * np.ones_like(matched), 1/len(matched) * np.ones_like(sig), 1/len(back) * np.ones_like(back)], histtype='step', label=["matched", "wrongly matched", "unmatchable"])
            else:
                #hist, _, _ = ax.hist([matched, sig, back], bins = 100, weights=[1/(len(matched) + len(sig)) * np.ones_like(matched), 1/(len(matched) + len(sig)) * np.ones_like(sig), 1/len(back) * np.ones_like(back)], histtype='step', label=["matched", "wrongly matched", "unmatchable"])
                n, _, _ = hist, _, _ = ax.hist([matched, sig, back], bins = 100, weights=[1/len(matched) * np.ones_like(matched), 1/len(sig) * np.ones_like(sig), 1/len(back) * np.ones_like(back)], histtype='step', label=["matched", "wrongly matched", "unmatchable"])
            bot, top = ax.get_ylim()
            ax.set_ylim(bot, top * 1.3)
            ax.set_ylabel(f"Normalized events")
            ax.set_xlabel(variables[var].format(r"{" + f"{rename[res_par]}" + r"}"))
            ax.legend(loc="best")
            fig.savefig(output_folder + f"/{res_par}_{var}_{WP}_OLD.pdf")
            created_pdfs.append(output_folder + f"/{res_par}_{var}_{WP}.pdf")
            plt.clf()

    plot_names = ["th", "tl", "H", "th tl", "th H", "tl H", "th tl H"]
    matching_fraction = {key: [] for key in plot_names}
    reco = {key: [] for key in plot_names}
    purity = {key: [] for key in plot_names}
    for key in plot_names:
        C = np.ones_like(dic["th"]["matchable"])
        U = np.zeros_like(C)
        for res_par in key.split(" "):
            C &= dic[res_par]["matched"]
            U |= dic[res_par]["unmatchable"]
        W = ~C & ~U

        C = dic["th"]["weight"][C]
        U = dic["th"]["weight"][U]
        W = dic["th"]["weight"][W]

        if key == "H":
            print(len(C))
            print(len(W))
            print(len(U))
        C_var = np.sum(C ** 2)
        C = np.sum(C)
        U_var = np.sum(U ** 2)
        U = np.sum(U)
        W_var = np.sum(W ** 2)
        W = np.sum(W)
        if key == "H":
            print(C)
            print(W)
            print(U)

        reco[key], matching_fraction[key], purity[key] =  calc_reco_mf_purity(C, W, U, C_var, W_var, U_var)


    efficiencies = {"reco": reco, "matching_fraction": matching_fraction, "purity": purity}
    for ukey, dics in efficiencies.items():
        fig, ax = plt.subplots(figsize=(7.2, 4.8))
        keys = dics.keys()
        vals = np.flip(np.vstack([dics[key] for key in keys]), axis=0)
        keys = [r"$" + key.replace("h", r"_{\mathrm{had}}").replace("l", r"_{\mathrm{lep}}").replace(" ", "+") + r"$" for key in keys]
        keys.reverse()
        ax.errorbar(keys, vals[:, 0], yerr=vals[:, 1], marker='x', fmt=" ")
        if ukey == "reco":
            ax.set_ylabel(r"$\varepsilon_{\mathrm{reco}}$")
        elif ukey == "matching_fraction":
            ax.set_ylabel(r"$\varepsilon_{\mathrm{match}}$")
        elif ukey == "purity":
            ax.set_ylabel("Purity")
        ax.set_ylim(0.0, 1)
        ax.set_yticks(np.linspace(0, 1, 11))
        ax.minorticks_on()
        ax.tick_params(axis='x', which='minor', bottom=False)
        ax.grid(b=True, which='both', axis='y')
        ax.grid(b=True, which='major', axis='x', linestyle='--')
        fig.savefig(output_folder + f"/{ukey}_{WP}.pdf")
        plt.clf()

    out_dict_name = output_folder + '/efficiencies_' + input_file.split("_")[-1]
    print(f"\nProcessed file: {input_file}")
    print(f"\nSafe efficiencies to {out_dict_name}\n")
    np.save(output_folder + "/efficiencies_" + input_file.split("_")[-1], efficiencies)
    print("key\tmf\ts_mf\treco\ts_reco\tpuri\ts_puri")
    for key in plot_names:
        print(f"{key}\t{matching_fraction[key][0]:.3f}\t{matching_fraction[key][1]:.3f}\t{reco[key][0]:.3f}\t{reco[key][1]:.3f}\t{purity[key][0]:.3f}\t{purity[key][1]:.3f}")
    command = ["pdfunite"] + created_pdfs + [output_folder + "/plots.pdf"]
    run(command)

    log_file = glob.glob(log_folder + 'events.out.tfevents.*')
    assert len(log_file) == 1
    log_file = log_file[0]


    saved_checkpoint = int(glob.glob(log_folder + "/checkpoints/epoch*")[0].split("=")[1].split("-")[0])

    # Needed since the names are kind of random
    scalars = scalars_of_interest()
    all_scalars = []
    for e in summary_iterator(log_file):
        for v in e.summary.value:
            if v.tag not in all_scalars:
                all_scalars.append(v.tag)

    if all(scale in all_scalars for scale in scalars_of_interest_ht_lt()):
        scalars.update(scalars_of_interest_ht_lt())
    elif all(scale in all_scalars for scale in scalars_of_interest_th_lt()):
        scalars.update(scalars_of_interest_th_lt())
    elif all(scale in all_scalars for scale in scalars_of_interest_ht_tl()):
        scalars.update(scalars_of_interest_ht_tl())
    elif all(scale in all_scalars for scale in scalars_of_interest_th_tl()):
        scalars.update(scalars_of_interest_th_tl())
    else:
        raise ValueError("Error in names for dict of scalars!")
    
    for e in summary_iterator(log_file):
        for v in e.summary.value:
            if v.tag in scalars:
                scalars[v.tag].append(round(v.simple_value, 3))

    scalars = {key: liste for key, liste in scalars.items() if len(liste) > 0}

    created_pdfs = []
    for i, (key, liste) in enumerate(scalars.items()):
        if "ht" in key:
            key = key.replace("ht", "th")
        if "lt" in key:
            key = key.replace("lt", "tl")
        fig, axs = plt.subplots()
        axs.plot(np.arange(1, len(liste) + 1), liste)
        # Plot line to indicate "best" network and add value at this point
        axs.axvline(saved_checkpoint + 1, alpha=0.7, linestyle='--')
        axs.text(0.8, 0.9, liste[saved_checkpoint], verticalalignment="bottom", transform=axs.transAxes)
        #axs.set_ylabel(key.split("/")[-1])
        axs.set_ylabel(key)
        axs.set_xlabel("Epoch")
        bot, top = axs.get_ylim()
        axs.set_ylim(bot, top * 1.2)
        fig.tight_layout()
        name = key.replace("/", "-")
        created_pdfs.append(output_folder + "/" + name + ".pdf")
        fig.savefig(output_folder + "/" + name + ".pdf")
        plt.clf()
        plt.close(fig)
    command = ["pdfunite"] + created_pdfs + [output_folder + "/logs.pdf"]
    run(command)



def scalars_of_interest() -> dict:
    keys = ("epoch/validation_loss",
            "epoch/loss",
            "epoch/validation_accuracy",
            "accuracy",
            "validation/jet/accuracy_1_of_1",
            "validation/jet/accuracy_1_of_2",
            "validation/jet/accuracy_2_of_2",
            "validation/jet/accuracy_1_of_3",
            "validation/jet/accuracy_2_of_3",
            "validation/jet/accuracy_3_of_3",
            "validation/particle/accuracy_1_of_1",
            "validation/particle/accuracy_1_of_2",
            "validation/particle/accuracy_2_of_2",
            "validation/particle/accuracy_1_of_3",
            "validation/particle/accuracy_2_of_3",
            "validation/particle/accuracy_3_of_3",
            "training/jet/accuracy_1_of_1",
            "training/jet/accuracy_1_of_2",
            "training/jet/accuracy_2_of_2",
            "training/jet/accuracy_1_of_3",
            "training/jet/accuracy_2_of_3",
            "training/jet/accuracy_3_of_3",
            "training/particle/accuracy_1_of_1",
            "training/particle/accuracy_1_of_2",
            "training/particle/accuracy_2_of_2",
            "training/particle/accuracy_1_of_3",
            "training/particle/accuracy_2_of_3",
            "training/particle/accuracy_3_of_3")
    scalars = {key: [] for key in keys}
    return scalars

def scalars_of_interest_ht_lt() -> dict:
    keys = ("Purity/*ht*lt1H/H_purity",
            "Purity/*ht*lt1H/event_proportion",
            "Purity/*ht1lt*H/lt_purity",
            "Purity/*ht1lt*H/event_proportion",
            "Purity/1ht*lt*H/ht_purity",
            "Purity/1ht*lt*H/event_proportion",
            "Purity/1ht1lt1H/ht_purity",
            "Purity/1ht1lt1H/lt_purity",
            "Purity/1ht1lt1H/H_purity",
            "Purity/1ht1lt1H/event_proportion",
            "Purity/0ht0lt0H/event_proportion",
            "Purity/0ht0lt0H/event_proportion")
    scalars = {key: [] for key in keys}
    return scalars


def scalars_of_interest_th_tl() -> dict:
    keys = ("Purity/*th*tl1H/H_purity",
            "Purity/*th*tl1H/event_proportion",
            "Purity/*th1tl*H/tl_purity",
            "Purity/*th1tl*H/event_proportion",
            "Purity/1th*tl*H/th_purity",
            "Purity/1th*tl*H/event_proportion",
            "Purity/1th1tl1H/th_purity",
            "Purity/1th1tl1H/tl_purity",
            "Purity/1th1tl1H/H_purity",
            "Purity/1th1tl1H/event_proportion",
            "Purity/0th0tl0H/event_proportion",
            "Purity/0th0tl0H/event_proportion")
    scalars = {key: [] for key in keys}
    return scalars


def scalars_of_interest_th_lt() -> dict:
    keys = ("Purity/*th*lt1H/H_purity",
            "Purity/*th*lt1H/event_proportion",
            "Purity/*th1lt*H/lt_purity",
            "Purity/*th1lt*H/event_proportion",
            "Purity/1th*lt*H/th_purity",
            "Purity/1th*lt*H/event_proportion",
            "Purity/1th1lt1H/th_purity",
            "Purity/1th1lt1H/lt_purity",
            "Purity/1th1lt1H/H_purity",
            "Purity/1th1lt1H/event_proportion",
            "Purity/0th0lt0H/event_proportion",
            "Purity/0th0lt0H/event_proportion")
    scalars = {key: [] for key in keys}
    return scalars

def scalars_of_interest_ht_tl() -> dict:
    keys = ("Purity/*ht*tl1H/H_purity",
            "Purity/*ht*tl1H/event_proportion",
            "Purity/*ht1tl*H/tl_purity",
            "Purity/*ht1tl*H/event_proportion",
            "Purity/1ht*tl*H/ht_purity",
            "Purity/1ht*tl*H/event_proportion",
            "Purity/1ht1tl1H/ht_purity",
            "Purity/1ht1tl1H/tl_purity",
            "Purity/1ht1tl1H/H_purity",
            "Purity/1ht1tl1H/event_proportion",
            "Purity/0ht0tl0H/event_proportion",
            "Purity/0ht0tl0H/event_proportion")
    scalars = {key: [] for key in keys}
    return scalars


def calc_reco_mf_purity(C, W, U, C_var, W_var, U_var, debug=False):
    """ Given values for C (correctly matched), W (wrongly matched) and U (unmatchable),
    calculates the reconstruction efficiency, matching fraction and purity and their errors.
    C_var is the variance on C, W_var on W and U_var on U. """

    reco = np.zeros(2)
    matching_fraction = np.zeros(2)
    purity = np.zeros(2)
    reco[0] = C / (C + W)
    matching_fraction[0] = (C + W) / (U + C + W)
    purity[0] = C / (U + C + W)

    reco[1] = np.sqrt((W / (C + W)**2 * np.sqrt(C_var))**2 +
                           (C / (C + W)**2 * np.sqrt(W_var))**2)
    matching_fraction[1] = np.sqrt((U / (U + C + W)**2 * np.sqrt(C_var))**2 +
                                   (U / (U + C + W)**2 * np.sqrt(W_var))**2 +
                                   ((C + W) / (U + C + W)**2 * np.sqrt(U_var))**2)
    purity[1] = np.sqrt(((U + W) / (U + C + W)**2 * np.sqrt(C_var))**2 +
                             (C / (U + C + W)**2 * np.sqrt(W_var))**2 +
                             (C / (U + C + W)**2 * np.sqrt(U_var))**2)
    return reco, matching_fraction, purity

def plot_var_distro(x_label, output_name, values, weights, bins, fig=None, ax=None, y_label="Normalized events", labels=["matched correctly", "matched wrongly", "unmatchable"]):
    if fig is None:
        fig, ax = plt.subplots()
    bin_edges = None
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    ranges=None
    if "mass" in output_name:
        range=(0, 300)
    for i, val in enumerate(values):
        if bin_edges is None: 
            vals_unweighted, bin_edges = np.histogram(val, bins, range=ranges)
        else:
            vals_unweighted, _ = np.histogram(val, bin_edges)

        vals_weighted, _ = np.histogram(val, bin_edges, weights[i])

        bin_centres = np.diff(bin_edges)

        vals_weighted = np.array(vals_weighted, dtype=np.float, copy=False)
        vals_unweighted = np.array(vals_weighted, dtype=np.float, copy=False)
        unc = np.divide(vals_weighted, np.sqrt(vals_unweighted), out=np.zeros_like(vals_weighted), where=vals_unweighted>0) / len(val)

        n, _, _ = ax.hist(bin_edges[:-1], bin_edges, weights = vals_weighted / len(val), histtype='step', stacked = False, fill = False, label=labels[i] + f" (N={len(val)})", color=colors[i])
        #ax.hist(bin_edges[:-1], bin_edges, histtype='step', stacked = False, fill = False, label=labels[i] + f" (N={len(val)})", color=colors[i])
        ax.hist(bin_centres, bin_edges, bottom=vals_weighted / len(val) - unc, weights= unc * 2, fill = False, hatch = "//////", linewidth=0, color=colors[i])

    bot, top = ax.get_ylim()
    ax.set_ylim(bot, top * 1.3)
    ax.set_ylabel(y_label)
    ax.set_xlabel(x_label)
    atlas.makeSimulationtag(ax, fig, output_name.split("_")[-1].split(".")[0])
    ax.legend(loc="best")
    fig.savefig(output_name)
    plt.clf()
    plt.close(fig)







if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-if", "--input_file", type=str, default="", help="The npy file created by process_results.py")
    parser.add_argument("-of", "--output_folder", type=str, default="", help="Folder where plots are saved.")
    parser.add_argument("-lf", "--log_folder", type=str, help="Folder where logs were saved.")
    arguments = parser.parse_args()
    main(**arguments.__dict__)
