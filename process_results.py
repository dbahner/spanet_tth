import re
import collections as colle
from argparse import ArgumentParser
import json
import h5py
import numpy as np
from uproot_methods import TLorentzVectorArray as tlv
from spanet.dataset.event_info import EventInfo


def main(input_file: str,
         test_file: str,
         event_file: str,
         output_file: str):
    # Create EventInfo object from ini file
    event_info = EventInfo.read_from_ini(event_file)
    pattern = re.compile(r'\(([^)]+)\)')
    

    # First get the predictions from the input file
    predictions = {}
    scores = {}
    with h5py.File(input_file, "r") as f:
        for par in event_info.event_particles:
            predictions.update({par: {}})
            for key in f[par]:
                if key == "mask":
                    continue
                if key == "score":
                    scores.update({par: f[par][key][:]})
                    continue
                predictions[par].update({key: f[par][key][:]})

    # Next get the predicted four vectors. The values used for plotting are stored in out_dict.
    four_vectors = {key: dict.fromkeys(predictions[key].keys()) for key in predictions.keys()}
    for key in four_vectors:
        four_vectors[key].update(dict.fromkeys(["matchable", "matched", "four_vec"]))
    out_dict = {}

    with h5py.File(test_file, "r") as f:
        # Loop over all particles
        for particle in predictions:
            # Get the symmetries from the event info and transform them into array of symmetries
            perms = pattern.findall(event_info.targets[particle][1])
            for i, perm in enumerate(perms):
                perms[i] = perm.split(', ')

            matchable = f[particle]["mask"][:]
            unmatchable = ~matchable
            matched = np.ones(len(f[particle]["mask"]), dtype=bool)
            for parton in predictions[particle]:
                # If there are symmetries, take care that matched will be correct.
                parton_perm = None
                for perm in perms:
                    if parton in perm:
                        parton_perm = perm

                # Check if parton has been assigned correctly
                if parton_perm is None:
                    cond = np.equal(predictions[particle][parton], f[particle][parton][:])
                else:
                    cond = np.zeros(len(f[particle]["mask"]), dtype=bool)
                    for perm in parton_perm:
                        cond = cond | np.equal(predictions[particle][parton], f[particle][perm][:])

                # Set all entries to False where parton has been assigned wrongly
                matched[~cond] = False
                pt = f["source"]["pt"][:][np.arange(len(f["source"]["pt"][:])), predictions[particle][parton]]
                eta = f["source"]["eta"][:][np.arange(len(f["source"]["eta"][:])), predictions[particle][parton]]
                phi = f["source"]["phi"][:][np.arange(len(f["source"]["phi"][:])), predictions[particle][parton]]
                mass = f["source"]["mass"][:][np.arange(len(f["source"]["mass"][:])), predictions[particle][parton]]
                four_vectors[particle][parton] = tlv.from_ptetaphim(pt, eta, phi, mass)

            
            # Ugly workaround to sum up all TLorentzVectorArray from one resonance particle
            fv = None
            for parton in predictions[particle]:
                if fv is None:
                    fv = four_vectors[particle][parton]
                else:
                    fv = fv + four_vectors[particle][parton]


            #four_vectors[particle]["four_vec"] =  fv
            #four_vectors[particle]["matched"] = matched
            #four_vectors[particle]["matchable"] = matchable
            out_dict.update({particle: {}})
            out_dict[particle].update({"matchable": matchable, "matched": matched, "unmatchable": unmatchable, "weight": f[particle]["weight"][:], "score": scores[particle]})
            out_dict[particle].update({"mass": fv.mass, "pt": fv.pt, "e": fv.energy, "eta": fv.eta, "phi": fv.phi})



        np.save(output_file, out_dict)





if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("input_file", type=str,
                        help="The HDF5 file created by predict.py.")

    parser.add_argument("test_file", type=str, 
                        help="The original test file to compare signal and background.")

    parser.add_argument("event_file", type=str, 
                        help="The event_ini file.")

    parser.add_argument("output_file", type=str,
                        help="The file where the output will be saved.")

    arguments = parser.parse_args()
    main(**arguments.__dict__)

