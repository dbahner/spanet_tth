import os
import h5py
import warnings
import uproot as up
import numpy as np
from argparse import ArgumentParser
from uproot_methods import TLorentzVectorArray, TLorentzVector
from tqdm import tqdm

# ======================================================================= #
#                           For testing only                              #
# ======================================================================= #
h_lj_more_than_once = 0
h_slj_more_than_once = 0
w_lj_more_than_once = 0
w_slj_more_than_once = 0
b_t_lep_more_than_once = 0
b_t_had_more_than_once = 0
# ======================================================================= #

def main(input_files: str,
         out_path: str,
         split_vals: str):
    split_vals = split_vals.split(',')
    split_vals = [float(split) for split in split_vals]
    if sum(split_vals) != 100:
        raise ValueError("Split values need to sum up to 1.")

    val_test = True
    if len(split_vals) > 3:
        raise ValueError("Only support for training,validating,testing or training,validating. More than 3 vals were given.")
    elif len(split_vals) == 2:
        val_test = False
    elif len(split_vals) < 2:
        raise ValueError("Only support for training,validating,testing or training,validating. Less than 2 vals were given.")

        
    # ======================================================================= #
    #                               Assumption                                #
    # The maximum jet multiplicity is 17, adding a lepton and neutrino raises #
    #                   this to a max lenght of 19.                           #
    # ======================================================================= #
    
    max_mul = 19

    # Load all variables as a dic
    var = variable_dic()
    # paths to the rootfiles are saved in a txt file, each row is one path
    rootfile_paths = open(input_files, 'r')
    input_paths = []
    for path in rootfile_paths:
        input_paths.append(path)
    rootfile_paths.close()


    passed = {85: [], 77: [], 70: [], 60: []}
    # Loop over all files
    for path in input_paths:
        print(f"Processing file {path}")
        # Open the tree_dic
        tree = up.open(path.rstrip('\n'))['nominal_Loose']
        tree_dic = tree.arrays(list(var.values()))

        # Only process events that pass the selection
        evts = []
        #for evt in range(len(tree)):
        print("Get events that pass selection")
        for evt in tqdm(range(len(tree))):
            if not selection_criteria(tree_dic[var["mc"]][evt],
                                      tree_dic[var["nJets"]][evt],
                                      tree_dic[var["nBTags"]][evt],
                                      tree_dic[var["n_mu"]][evt],
                                      tree_dic[var["n_el"]][evt],
                                      tree_dic[var["truthmatch"]][evt]):
                continue
            evts.append(evt)
            passed[85].append(True)
            passed[77].append(tree_dic[var["nBTags_77"]][evt] > 3)
            passed[70].append(tree_dic[var["nBTags_70"]][evt] > 3)
            passed[60].append(tree_dic[var["nBTags_60"]][evt] > 3)
        if os.path.isfile(out_path + path.split("/")[-1].split(".")[0] + ".h5"):
            continue

        jet_fv = TLorentzVectorArray.from_ptetaphie(tree_dic[var["pt"]][evts] / 1000, tree_dic[var["eta"]][evts], tree_dic[var["phi"]][evts], tree_dic[var["e"]][evts] / 1000)
        jet_masses = jet_fv.mass
        jet_fv = 0

        jet_pt = tree_dic[var["pt"]][evts] / 1000
        jet_eta = tree_dic[var["eta"]][evts]
        jet_phi = tree_dic[var["phi"]][evts]
        n_jets = tree_dic[var["nJets"]][evts]
        isbtagged = tree_dic[var["isbtagged"]][evts]
        truthmatches = tree_dic[var["truthmatch"]][evts]

        el_fv = TLorentzVectorArray.from_ptetaphie(tree_dic[var["pt_el"]][evts] / 1000, tree_dic[var["eta_el"]][evts], tree_dic[var["phi_el"]][evts], tree_dic[var["e_el"]][evts] / 1000)
        pt_el = tree_dic[var["pt_el"]][evts] / 1000
        eta_el = tree_dic[var["eta_el"]][evts]
        phi_el = tree_dic[var["phi_el"]][evts]
        n_el = tree_dic[var["n_el"]][evts]
        
        mu_fv = TLorentzVectorArray.from_ptetaphie(tree_dic[var["pt_mu"]][evts] / 1000, tree_dic[var["eta_mu"]][evts], tree_dic[var["phi_mu"]][evts], tree_dic[var["e_mu"]][evts] / 1000)
        pt_mu = tree_dic[var["pt_mu"]][evts] / 1000
        eta_mu = tree_dic[var["eta_mu"]][evts]
        phi_mu = tree_dic[var["phi_mu"]][evts]
        n_mu = tree_dic[var["n_mu"]][evts]

        met_met = tree_dic[var["met_met"]][evts] / 1000
        phi_met = tree_dic[var["phi_met"]][evts]
        n_evts = len(evts)

        total_weight = np.prod(np.vstack([tree_dic[val][evts] for _, val in weight_dic().items()]), axis=0)
        tree_dic = 0

        masses = np.zeros((n_evts, max_mul), dtype=np.float32)
        pts = np.zeros((n_evts, max_mul), dtype=np.float32)
        etas = np.zeros((n_evts, max_mul), dtype=np.float32)
        phis = np.zeros((n_evts, max_mul), dtype=np.float32)
        btags = np.zeros((n_evts, max_mul), dtype=np.float32)
        lep_tags = np.zeros((n_evts, max_mul), dtype=np.float32)
        masks = np.zeros((n_evts, max_mul), dtype=bool)

        thad_mask = np.zeros(n_evts, dtype=bool)
        thad_b = -np.ones(n_evts, dtype=int)
        thad_q1 = -np.ones(n_evts, dtype=int)
        thad_q2 = -np.ones(n_evts, dtype=int)

        tlep_mask = np.zeros(n_evts, dtype=bool)
        tlep_b = -np.ones(n_evts, dtype=int)
        tlep_lep = -np.ones(n_evts, dtype=int)
        tlep_nu = -np.ones(n_evts, dtype=int)

        higgs_mask = np.zeros(n_evts, dtype=bool)
        higgs_b1 = -np.ones(n_evts, dtype=int)
        higgs_b2 = -np.ones(n_evts, dtype=int)


        print("Fill numpy arrays")
        #for i in range(n_evts):
        for i in tqdm(range(n_evts)):
            # First fill information about the jets
            mask = np.concatenate((np.ones(n_jets[i], dtype=bool), np.zeros(max_mul - n_jets[i], dtype=bool)))
            masks[i] = mask
            masses[i][mask] = np.nan_to_num(jet_masses[i], copy=False, nan=0) # Mass squares can become negative, set to 0 
            pts[i][mask] = jet_pt[i]
            etas[i][mask] = jet_eta[i]
            phis[i][mask] = jet_phi[i]
            btags[i][mask] = isbtagged[i]
            
            # Next add the lepton and neutrino
            if n_mu[i]:
                nu_fv = get_neutrino_fv(met_met[i], phi_met[i], mu_fv[i])
                pt_lep = pt_mu[i]
                eta_lep = eta_mu[i]
                phi_lep = phi_mu[i]
                m_lep = 0.1057
            elif n_el[i]:
                nu_fv = get_neutrino_fv(met_met[i], phi_met[i], el_fv[i])
                pt_lep = pt_el[i]
                eta_lep = eta_el[i]
                phi_lep = phi_el[i]
                m_lep = 511e-6
            m_nu = 0

            masks[i][n_jets[i]:(n_jets[i] + 2)] = True, True
            masses[i][n_jets[i]:(n_jets[i] + 2)] = m_lep, m_nu
            pts[i][n_jets[i]:(n_jets[i] + 2)] = pt_lep, met_met[i]
            etas[i][n_jets[i]:(n_jets[i] + 2)] = eta_lep, nu_fv.eta
            phis[i][n_jets[i]:(n_jets[i] + 2)] = phi_lep, phi_met[i]
            lep_tags[i][n_jets[i]:(n_jets[i] + 2)] = True, True

            # Fill true pos of jets
            higgs, t_lep, t_had = check_truthmatch(truthmatches[i])
            
            if -1 in higgs:
                pass
            else:
                higgs_mask[i] = True
            higgs_b1[i], higgs_b2[i] = higgs

            if -1 in t_had:
                pass
            else:
                thad_mask[i] = True
            thad_b[i], thad_q1[i], thad_q2[i] = t_had

            if -1 in t_lep:
                pass
            else:
                tlep_mask[i] = True
            tlep_b[i] = t_lep[0]
            tlep_lep[i] = n_jets[i]
            tlep_nu[i] = n_jets[i] + 1


        print("Save h5 files")
        # Save each root file seperately as h5 file and merge them later
        f = h5py.File(out_path + path.split("/")[-1].split(".")[0] + ".h5", "w")
        source = f.create_group("source")
        btag = source.create_dataset("btag", data=btags)
        lep_tag = source.create_dataset("leptag", data=lep_tags)
        eta = source.create_dataset("eta", data=etas)
        mask = source.create_dataset("mask", data=masks)
        pt = source.create_dataset("pt", data=pts)
        phi = source.create_dataset("phi", data=phis)
        mass = source.create_dataset("mass", data=masses)

        t_had = f.create_group("th")
        t_had_mask = t_had.create_dataset("mask", data=thad_mask)
        b_t_had = t_had.create_dataset("b", data=thad_b)
        q1 = t_had.create_dataset("q1", data=thad_q1)
        q2 = t_had.create_dataset("q2", data=thad_q2)
        weight = t_had.create_dataset("weight", data=total_weight)

        t_lep = f.create_group("tl")
        t_lep_mask = t_lep.create_dataset("mask", data=tlep_mask)
        b_t_lep = t_lep.create_dataset("b", data=tlep_b)
        lep = t_lep.create_dataset("lep", data=tlep_lep)
        nu = t_lep.create_dataset("nu", data=tlep_nu)
        weight = t_lep.create_dataset("weight", data=total_weight)


        higgs = f.create_group("H")
        higgs_mask = higgs.create_dataset("mask", data=higgs_mask)
        higgs_b1 = higgs.create_dataset("b1", data=higgs_b1)
        higgs_b2 = higgs.create_dataset("b2", data=higgs_b2)
        weight = higgs.create_dataset("weight", data=total_weight)

        f.close()

    files = []
    for path in input_paths:
        files.append(h5py.File(out_path + path.split("/")[-1].split(".")[0] + ".h5", "r"))

    for WP in passed:
        out_file_train = h5py.File(f"{out_path}/ttbarhiggs_training_{WP}.h5", "w")
        out_file_val = h5py.File(f"{out_path}/ttbarhiggs_validation_{WP}.h5", "w")
        out_file_test = h5py.File(f"{out_path}/ttbarhiggs_testing_{WP}.h5", "w")
        out_file_test_weights = h5py.File(f"{out_path}/ttbarhiggs_testing_{WP}_weights.h5", "w")
        for key in files[0]:
            print(key)
            train = out_file_train.create_group(key)
            val = out_file_val.create_group(key)
            test = out_file_test.create_group(key)
            test_weights = out_file_test_weights.create_group(key)
            print("key\ttraining\tvalidating\ttesting")
            for okey in files[0][key]:
                data = np.concatenate([f[key][okey][:] for f in files])
                np.random.seed(42)
                shuffle = np.arange(len(data))
                np.random.shuffle(shuffle)
                training_end = int(len(shuffle) * split_vals[0] / 100)
                validating_end = training_end + int(len(shuffle) * split_vals[1] / 100)
                try:
                    training = shuffle[:training_end][np.array(passed[WP], copy=False)[shuffle][:training_end]]
                    validating = shuffle[training_end:validating_end][np.array(passed[WP], copy=False)[shuffle][training_end:validating_end]]
                    testing = shuffle[validating_end:][np.array(passed[WP], copy=False)[shuffle][validating_end:]]
                except TypeError:
                    print(training_end)
                    print(len(np.array(passed[WP], copy=False)))
                    print(len(np.array(passed[WP], copy=False)[shuffle]))
                    print(len(np.array(passed[WP], copy=False)[shuffle][:training_end]))


                if okey == "weight":
                    val_test_weights = test_weights.create_dataset(okey, data=data[testing])
                else:    
                    val_train = train.create_dataset(okey, data=data[training])
                    val_val = val.create_dataset(okey, data=data[validating])
                    val_test = test.create_dataset(okey, data=data[testing])
                    val_test_weights = test_weights.create_dataset(okey, data=data[testing])
                print(f"{okey}\t{len(training)}\t{len(validating)}\t{len(testing)}")

        out_file_train.close()
        out_file_val.close()
        out_file_test.close()
        out_file_test_weights.close()
    for f in files:
        f.close()

    





def variable_dic() -> dict:
    """Returns all variables that are read from the root file"""
    dic = {}
    vars_selection = {"mc": b"mcChannelNumber", "nJets": b"nJets", "nBTags": b"nBTags_DL1r_85", "nBTags_77": b"nBTags_DL1r_77", "nBTags_70": b"nBTags_DL1r_70", "nBTags_60": b"nBTags_DL1r_60"}
    jet_quantities = {"pt": b"jet_pt", "e": b"jet_e", "eta": b"jet_eta", "phi": b"jet_phi", "truthmatch": b"jet_truthmatch", "isbtagged": b"isbtagged_DL1r_85"}
    lep_quantities = {"pt_mu": b"mu_pt", "e_mu": b"mu_e", "phi_mu": b"mu_phi", "eta_mu": b"mu_eta", "n_mu": b"nMuons",
                      "pt_el": b"el_pt", "e_el": b"el_e", "phi_el": b"el_phi", "eta_el": b"el_eta", "n_el": b"nElectrons",
                      "met_met": b"met_met", "phi_met": b"met_phi"}
    weights = {
        'weight_normalise': b'weight_normalise',
        'weight_mc': b'weight_mc',
        'weight_jvt': b'weight_jvt',
        'weight_leptonSF': b'weight_leptonSF',
        'weight_pileup': b'weight_pileup',
        'weight_bTagSF_DL1r_Continuous': b'weight_bTagSF_DL1r_Continuous'
    }
    dic.update(vars_selection)
    dic.update(jet_quantities)
    dic.update(lep_quantities)
    dic.update(weights)
    return dic


def weight_dic() -> dict:
    weights = {
        'weight_normalise': b'weight_normalise',
        'weight_mc': b'weight_mc',
        'weight_jvt': b'weight_jvt',
        'weight_leptonSF': b'weight_leptonSF',
        'weight_pileup': b'weight_pileup',
        'weight_bTagSF_DL1r_Continuous': b'weight_bTagSF_DL1r_Continuous'
    }
    return weights


def selection_criteria(mcChannelNumber: int, nJets: int, nBTags_DL1r_85: int, n_mu: int, n_el: int, truthmatch: np.array) -> bool:
    if mcChannelNumber != 346344:
        # Only semileptonic decays
        return False
    if nJets < 6:
        # At least 6 jets
        return False
    if nBTags_DL1r_85 < 4:
        # At least 4 btags at 85% WP
        return False
    if n_mu + n_el != 1:
        # Check that there is exactly one lepton (should be the case either way)
        return False

    h = -np.ones(2)
    t_lep = -np.ones(1)
    t_had = -np.ones(3)
    for i, flag in enumerate(truthmatch):
        if flag == 1:
            if h[0] == -1:
                h[0] = i
            else:
                return False
        if flag == 2:
            if h[1] == -1:
                h[1] = i
            else:
                return False
        if flag in (4100, 8196):
            if t_lep[0] == -1:
                t_lep[0] = i
            else:
                return False
        if flag in (4104, 8200):
            if t_had[0] == -1:
                t_had[0] = i
            else:
                return False
        if flag in (16400, 32784):
            if t_had[1] == -1:
                t_had[1] = i
            else:
                return False
        if flag in (16416, 32800):
            if t_had[2] == -1:
                t_had[2] = i
            else:
                return False
    return True
        
def check_truthmatch(flags: np.ndarray) -> (np.array, np.array, np.array):
    """ Checks if jet has a single truthmatch with a quark.
    1:      h1
    2:      h2
    4100:   b_lep from top
    8196:   b_lep from antitop
    4104:   b_had from top
    8200:   b_had from antitop
    16400:  q1+
    32784:  q1-
    16416:  q2+
    32800:  q2-
    """
    global h_lj_more_than_once
    global h_slj_more_than_once
    global w_lj_more_than_once
    global w_slj_more_than_once
    global b_t_lep_more_than_once
    global b_t_had_more_than_once

    h = -np.ones(2)
    t_lep = -np.ones(1)
    t_had = -np.ones(3)
    for i, flag in enumerate(flags):
        if flag == 1:
            if h[0] == -1:
                h[0] = i
            else:
                h_lj_more_than_once += 1
                #warnings.warn("More than one truthmatch for leading higgs!\nOccured for the " + str(h_lj_more_than_once) + " time.")
        if flag == 2:
            if h[1] == -1:
                h[1] = i
            else:
                h_slj_more_than_once += 1
                #warnings.warn("More than one truthmatch for subleading higgs!\nOccured for the " + str(h_slj_more_than_once) + " time.")
        if flag in (4100, 8196):
            if t_lep[0] == -1:
                t_lep[0] = i
            else:
                b_t_lep_more_than_once += 1
                #warnings.warn("More than one truthmatch for b_t_lep!\nOccured for the " + str(b_t_lep_more_than_once) + " time.")
        if flag in (4104, 8200):
            if t_had[0] == -1:
                t_had[0] = i
            else:
                b_t_had_more_than_once += 1
                #warnings.warn("More than one truthmatch for b_t_had!\nOccured for the " + str(b_t_had_more_than_once) + " time.")
        if flag in (16400, 32784):
            if t_had[1] == -1:
                t_had[1] = i
            else:
                w_lj_more_than_once += 1
                #warnings.warn("More than one truthmatch for leading W!\nOccured for the " + str(w_lj_more_than_once) + " time.")
        if flag in (16416, 32800):
            if t_had[2] == -1:
                t_had[2] = i
            else:
                w_slj_more_than_once += 1
                #warnings.warn("More than one truthmatch for subleading W!\nOccured for the " + str(w_slj_more_than_once) + " time.")
    return h, t_lep, t_had


def get_neutrino_fv(met_met: float, phi_met: float, lep_fv: TLorentzVector) -> TLorentzVector:
    """ Calculate the four momentum of the neutrino using the invariant mass of the W boson."""

    m_w = 80.385
    p_xnu = met_met * np.cos(phi_met)
    p_ynu = met_met * np.sin(phi_met)

    beta = m_w ** 2 - abs(lep_fv.mass2) + 2 * \
        (lep_fv.x * p_xnu + lep_fv.y * p_ynu)

    delta = lep_fv.energy ** 2 * \
        (beta ** 2 + (2 * lep_fv.z * met_met) ** 2 - (2 * lep_fv.energy * met_met) ** 2)

    if delta <= 0:
        # If there is no real solution, set discriminant to zero
        delta = 0

    p_znu = (lep_fv.z * beta + np.sqrt(delta)) / \
        (2 * (lep_fv.energy**2 - lep_fv.z**2))
    e_nu = np.sqrt(met_met**2 + p_znu**2)
    nu_fv = TLorentzVectorArray.from_cartesian(p_xnu, p_ynu, p_znu, e_nu)

    return nu_fv

if __name__=='__main__':
    parser = ArgumentParser()
    parser.add_argument('-if', '--input_files', type=str, default="", help="txt file with the path(s) to the root file(s)")
    parser.add_argument('-op', '--out_path', type=str, default="", help="path where file(s) should be saved")
    parser.add_argument('-sv', '--split_vals', type=str, default="70,15,15", help="Fraction of events used for training, validating and testing.")
    main(**parser.parse_args().__dict__)

