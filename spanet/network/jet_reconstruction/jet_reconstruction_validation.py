from typing import Dict

import numpy as np
import torch


from spanet.options import Options
from spanet.network.jet_reconstruction.jet_reconstruction_network import JetReconstructionNetwork


class JetReconstructionValidation(JetReconstructionNetwork):
    def __init__(self, options: Options):
        super(JetReconstructionValidation, self).__init__(options)

    def validation_step(self, batch, batch_idx) -> Dict[str, np.float32]:
        # Run the base prediction step
        (source_data, source_mask), *targets = batch
        jet_predictions, particle_scores = self.predict_jets_and_particle_scores(source_data, source_mask)


        batch_size = source_data.shape[0]
        num_targets = len(targets)

        # Stack all of the targets into single array, we will also move to numpy for easier the numba computations.
        stacked_targets = np.zeros(num_targets, dtype=object)
        stacked_masks = np.zeros((num_targets, batch_size), dtype=np.bool)
        for i, (target, mask) in enumerate(targets):
            stacked_targets[i] = target.detach().cpu().numpy()
            stacked_masks[i] = mask.detach().cpu().numpy()

        metrics = self.evaluator.full_report_string(jet_predictions, stacked_targets, stacked_masks, prefix="Purity/")

        # Apply permutation groups for each target
        for target, prediction, decoder in zip(stacked_targets, jet_predictions, self.decoders):
            for indices in decoder.permutation_indices:
                if len(indices) > 1:
                    prediction[:, indices] = np.sort(prediction[:, indices])
                    target[:, indices] = np.sort(target[:, indices])

        metrics.update(self.compute_metrics(jet_predictions, particle_scores, stacked_targets, stacked_masks))

        # Add validation loss to metrics
        with torch.no_grad():
            predictions = self.forward(source_data, source_mask)
        
        # Extract individual prediction data
        classifications = tuple(prediction[1] for prediction in predictions)
        predictions = tuple(prediction[0] for prediction in predictions)

        # ===================================================================================================
        # Initial log-likelihood loss for classification task
        # ---------------------------------------------------------------------------------------------------
        total_loss, best_indices = self.negative_log_likelihood(predictions, classifications, targets)

        # Construct the newly permuted masks based on the minimal permutation found during NLL loss.
        permutations = self.event_permutation_tensor[best_indices].T
        masks = torch.stack([target[1] for target in targets])
        masks = torch.gather(masks, 0, permutations)

        # ===================================================================================================
        # Auxiliary loss term to prevent distributions from collapsing into single output.
        # ---------------------------------------------------------------------------------------------------
        if self.options.kl_loss_scale > 0:
            # Compute the symmetric loss between all valid pairs of distributions.
            kl_loss = self.symmetric_divergence(predictions, masks)

            with torch.no_grad():
                self.log("loss/symmetric_loss", -kl_loss.mean())
                if torch.isnan(kl_loss).any():
                    raise ValueError("Symmetric KL Loss has diverged.")

            total_loss = total_loss - kl_loss * self.options.kl_loss_scale

        # ===================================================================================================
        # Balance the loss based on the distribution of various classes in the dataset.
        # ---------------------------------------------------------------------------------------------------

        # Balance based on the particles present - only used in partial event training
        if self.balance_particles:
            class_indices = (masks * self.particle_index_tensor.unsqueeze(1)).sum(0)
            class_weights = self.particle_weights_tensor[class_indices]
            total_loss = total_loss * class_weights

        # Balance based on the number of jets in this event
        if self.balance_jets:
            class_indices = source_mask.sum(1)
            class_weights = self.jet_weights_tensor[class_indices]
            total_loss = total_loss * class_weights

        # ===================================================================================================
        # Combine and save validation loss in metrics
        # ---------------------------------------------------------------------------------------------------

        total_loss = len(targets) * total_loss.sum() / masks.sum()
        #self.log("loss/validation_loss", total_loss)
        metrics["validation_loss"] = total_loss

        for name, value in metrics.items():
            try:
                if not np.isnan(value):
                    self.log(name, value)
            except TypeError:
                if not np.isnan(value.cpu().detach().numpy()):
                    self.log(name, value)


        return metrics

    def validation_epoch_end(self, outputs):
        if self.trainer.state.stage == "sanity_check":
            return
        # Optionally use this accuracy score for something like hyperparameter search
        # validation_accuracy = sum(x['validation_accuracy'] for x in outputs) / len(outputs)
        validation_accuracy = sum(x['validation_accuracy'] for x in outputs) / len(outputs)
        validation_loss = sum(x['validation_loss'] for x in outputs) / len(outputs)
        
        self.logger.experiment.add_scalar('epoch/validation_loss', validation_loss, self.current_epoch)
        self.logger.experiment.add_scalar('epoch/validation_accuracy', validation_accuracy, self.current_epoch)
        if self.options.verbose_output:
            for name, parameter in self.named_parameters():
                self.logger.experiment.add_histogram(name, parameter)

    def test_step(self, batch, batch_idx):
        return self.validation_step(batch, batch_idx)
