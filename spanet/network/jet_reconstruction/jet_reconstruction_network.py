from typing import Tuple, Dict, Callable

import numpy as np
import torch
from torch import Tensor, nn

from sklearn import metrics as sk_metrics

from spanet.network.jet_reconstruction.jet_reconstruction_base import JetReconstructionBase
from spanet.network.prediction_selection import extract_predictions
from spanet.network.layers.branch_decoder import BranchDecoder
from spanet.network.layers.jet_encoder import JetEncoder
from spanet.options import Options

from spanet.dataset.evaluator import SymmetricEvaluator

TArray = np.ndarray


class JetReconstructionNetwork(JetReconstructionBase):
    def __init__(self, options: Options):
        """ Base class defining the SPANet architecture.

        Parameters
        ----------
        options: Options
            Global options for the entire network.
            See network.options.Options
        """
        super(JetReconstructionNetwork, self).__init__(options)

        self.hidden_dim = options.hidden_dim

        # Shared options for all transformer layers
        transformer_options = (options.hidden_dim,
                               options.num_attention_heads,
                               options.hidden_dim,
                               options.dropout,
                               options.transformer_activation)

        self.encoder = JetEncoder(options, self.training_dataset.num_features, transformer_options)
        self.decoders = nn.ModuleList([
            BranchDecoder(options, size, permutation_indices, transformer_options, self.enable_softmax)
            for _, (size, permutation_indices) in self.training_dataset.target_symmetries
        ])

        # An example input for generating the network's graph, batch size of 2
        self.example_input_array = tuple(x.contiguous() for x in self.training_dataset[:2][0])

        self.evaluator = SymmetricEvaluator(self.training_dataset.event_info)

    @property
    def particle_metrics(self) -> Dict[str, Callable[[np.ndarray, np.ndarray], float]]:
        return {
            "accuracy": sk_metrics.accuracy_score,
            "sensitivity": sk_metrics.recall_score,
            "specificity": lambda t, p: sk_metrics.recall_score(~t, ~p),
            "f_score": sk_metrics.f1_score
        }

    @property
    def particle_score_metrics(self) -> Dict[str, Callable[[np.ndarray, np.ndarray], float]]:
        return {
            "roc_auc": sk_metrics.roc_auc_score,
            "average_precision": sk_metrics.average_precision_score
        }

    @property
    def enable_softmax(self):
        return True

    def forward(self, source_data: Tensor, source_mask: Tensor, debug: bool=False) -> Tuple[Tuple[Tensor, Tensor], ...]:
        # Normalize incoming data
        # This operation is gradient-free, so we can use inplace operations.
        source_data = source_data.clone()
        source_data[source_mask] -= self.mean
        source_data[source_mask] /= self.std

        if debug:
            print(source_data.detach().cpu().numpy())
            print(source_mask.detach().cpu().numpy())
        # Extract features from data using transformer
        hidden, padding_mask, sequence_mask = self.encoder(source_data, source_mask)
        if debug:
            with torch.no_grad():
                hidden, padding_mask, sequence_mask = self.encoder(source_data, source_mask, debug=debug)

        # Pass the shared hidden state to every decoder branch
        return tuple(decoder(hidden, padding_mask, sequence_mask, debug) for decoder in self.decoders)

    def predict_jets(self, source_data: Tensor, source_mask: Tensor) -> np.ndarray:
        # Run the base prediction step
        with torch.no_grad():
            predictions = []
            for prediction, _, _ in self.forward(source_data, source_mask):
                prediction[torch.isnan(prediction)] = -np.inf
                predictions.append(prediction)

            # Find the optimal selection of jets from the output distributions.
            return extract_predictions(predictions)

    def predict_jets_and_particle_scores(self, source_data: Tensor, source_mask: Tensor) -> Tuple[TArray, TArray]:
        with torch.no_grad():
            predictions = []
            scores = []
            for prediction, classification, _ in self.forward(source_data, source_mask):
                prediction[torch.isnan(prediction)] = -np.inf
                predictions.append(prediction)

                scores.append(torch.sigmoid(classification).cpu().numpy())

            return extract_predictions(predictions), np.stack(scores)

    def predict_jets_and_particles(self, source_data: Tensor, source_mask: Tensor) -> Tuple[TArray, TArray]:
        predictions, scores = self.predict_jets_and_particle_scores(source_data, source_mask)

        # Always predict the particle exists if we didn't train on it
        if self.options.classification_loss_scale == 0:
            scores += 1

        return predictions, scores >= 0.5

    def compute_metrics(self, jet_predictions, particle_scores, stacked_targets, stacked_masks):
        _state = self.trainer.state.stage
        if _state == "train":
            prefix = 'training/'
        elif _state == "validate":
            prefix = 'validation/'
        elif _state == "sanity_check":
            prefix = 'sanity_check/'
        event_permutation_group = self.event_permutation_tensor.cpu().numpy()
        num_permutations = len(event_permutation_group)
        num_targets, batch_size = stacked_masks.shape
        particle_predictions = particle_scores >= 0.5

        # Compute all possible target permutations and take the best performing permutation
        # First compute raw_old accuracy so that we can get an accuracy score for each event
        # This will also act as the method for choosing the best permutation to compare for the other metrics.
        jet_accuracies = np.zeros((num_permutations, num_targets, batch_size), dtype=np.bool)
        particle_accuracies = np.zeros((num_permutations, num_targets, batch_size), dtype=np.bool)
        for i, permutation in enumerate(event_permutation_group):
            for j, (prediction, target) in enumerate(zip(jet_predictions, stacked_targets[permutation])):
                jet_accuracies[i, j] = np.all(prediction == target, axis=1)

            particle_accuracies[i] = stacked_masks[permutation] == particle_predictions

        jet_accuracies = jet_accuracies.sum(1)
        particle_accuracies = particle_accuracies.sum(1)

        # Select the primary permutation which we will use for all other metrics.
        chosen_permutations = self.event_permutation_tensor[jet_accuracies.argmax(0)].T
        chosen_permutations = chosen_permutations.cpu()
        permuted_masks = torch.gather(torch.from_numpy(stacked_masks), 0, chosen_permutations).numpy()

        # Compute final accuracy vectors for output
        num_particles = stacked_masks.sum(0)
        jet_accuracies = jet_accuracies.max(0)
        particle_accuracies = particle_accuracies.max(0)

        # Create the logging dictionaries
        if not self.options.partial_events and self.trainer.state.stage == "train":
            metrics = {f"{prefix}jet/accuracy_{num_targets}_of_{num_targets}": (jet_accuracies[num_particles == num_targets] == num_targets).mean()}

            metrics.update({f"{prefix}particle/accuracy_{num_targets}_of_{num_targets}": (particle_accuracies[num_particles == num_targets] == num_targets).mean()})
        else:
            metrics = {f"{prefix}jet/accuracy_{i}_of_{j}": (jet_accuracies[num_particles == j] >= i).mean()
                       for j in range(1, num_targets + 1)
                       for i in range(1, j + 1)}

            metrics.update({f"{prefix}particle/accuracy_{i}_of_{j}": (particle_accuracies[num_particles == j] >= i).mean()
                            for j in range(1, num_targets + 1)
                            for i in range(1, j + 1)})


        particle_scores = particle_scores.ravel()
        particle_targets = permuted_masks.ravel()
        particle_predictions = particle_predictions.ravel()
        
        #if self.trainer.state.stage == "train":
        #    print("particle_targets:")
        #    print(particle_targets)
        #    print("particle_predictions:")
        #    print(particle_predictions)
        #    print("Invert particle_targets:")
        #    print(~particle_targets)
        #    print("Invert particle_predictions:")
        #    print(~particle_predictions)

        for name, metric in self.particle_metrics.items():
            if not self.options.partial_events and self.trainer.state.stage == "train":
                if name == 'specificity':
                    metrics[f"{prefix}particle/{name}"] = 0.0
                    continue
            metrics[f"{prefix}particle/{name}"] = metric(particle_targets, particle_predictions)
        #if self.trainer.state.stage == "train":
        #    print(~particle_targets == ~particle_predictions)
        #    exit()


        for name, metric in self.particle_score_metrics.items():
            if not self.options.partial_events:
                if name == "roc_auc":
                    if self.trainer.state.stage == "train":
                        # roc_auc does not make sense when training only on full events. 
                        # Set to 0 in this case.
                        metrics[f"{prefix}particle/{name}"] = 0
                        continue 
            metrics[f"{prefix}particle/{name}"] = metric(particle_targets, particle_scores)

        # Compute the sum accuracy of all complete events to act as our target for
        # early stopping, hyperparameter optimization, learning rate scheduling, etc.
        if self.trainer.training:
            metrics["accuracy"] = metrics[f"{prefix}jet/accuracy_{num_targets}_of_{num_targets}"]
        elif self.trainer.validating:
            metrics["validation_accuracy"] = metrics[f"{prefix}jet/accuracy_{num_targets}_of_{num_targets}"]

        return metrics
