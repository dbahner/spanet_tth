from typing import Tuple, Dict

import sys
import numpy as np
import torch
from torch import Tensor
from torch.nn import functional as F

from spanet.options import Options
from spanet.network.jet_reconstruction.jet_reconstruction_network import JetReconstructionNetwork
from spanet.network.utilities.divergence_losses import jet_cross_entropy_loss, jensen_shannon_divergence


class JetReconstructionTraining(JetReconstructionNetwork):
    def __init__(self, options: Options):
        super(JetReconstructionTraining, self).__init__(options)

        self.log_clip = torch.log(10 * torch.tensor(torch.finfo(torch.float32).eps)).item()
        self.metrics_training = None
        self.running_loss = []

    def particle_classification_loss(self, classification: Tensor, target_mask: Tensor) -> Tensor:
        loss = F.binary_cross_entropy_with_logits(classification, target_mask.float(), reduction='none')
        return self.options.classification_loss_scale * loss

    def negative_log_likelihood(self,
                                predictions: Tuple[Tensor, ...],
                                classifications: Tuple[Tensor, ...],
                                targets: Tuple[Tensor, ...], debug: bool=False) -> Tuple[Tensor, Tensor]:
        # We are only going to look at a single prediction points on the distribution for more stable loss calculation
        # We multiply the softmax values by the size of the permutation group to make every target the same
        # regardless of the number of sub-jets in each target particle
        predictions = [prediction + torch.log(torch.scalar_tensor(decoder.num_targets))
                       for prediction, decoder in zip(predictions, self.decoders)]

        # Convert the targets into a numpy array of tensors so we can use fancy indexing from numpy
        targets = np.array(targets, dtype='O')

        # Compute the loss on every valid permutation of the targets
        # TODO think of a way to avoid this memory transfer but keep permutation indices synced with checkpoint
        losses = []
        for permutation in self.event_permutation_tensor.cpu().numpy():
            loss = tuple(jet_cross_entropy_loss(P, T, M, self.options.focal_gamma) +
                         self.particle_classification_loss(C, M)
                         for P, C, (T, M)
                         in zip(predictions, classifications, targets[permutation]))
            if debug:
                pass
                #for P, C, (T, M) in zip(predictions, classifications, targets[permutation]):
                #    loss = jet_cross_entropy_loss(P, T, M, self.options.focal_gamma)
                #    print(P)
                #    #print(T)
                #    #print(M)
                #    print(loss)
                #    #for pred, classi, tar, lossi in zip(P, C, T, loss):
                #    #    print(pred,  classi, tar, lossi)
                #    print()


            losses.append(torch.sum(torch.stack(loss), dim=0))

        losses = torch.stack(losses)

        # Squash the permutation losses into a single value.
        # Typically we just take the minimum, but it might
        # be interesting to examine other methods.
        combined_losses, index = losses.min(0)

        if np.any(combined_losses.cpu().detach().numpy() > 1e3) and self.trainer.state.stage == "validate":
            #big_loss = np.where(combined_losses.cpu().detach().numpy() > 1e3)
            big_loss = np.argmax(combined_losses.cpu().detach().numpy())
            #print(big_loss)
            #print(big_loss[0])
            #print(combined_losses.cpu().detach().numpy()[big_loss])
            for permutation in self.event_permutation_tensor.cpu().numpy():
                for P, C, (T, M) in zip(predictions, classifications, targets[permutation]):
                    loss = tuple(jet_cross_entropy_loss(P, T, M, self.options.focal_gamma, verbose=True) +
                                 self.particle_classification_loss(C, M))
                    #print("loss:")
                    #print(torch.stack(loss, dim=0).cpu().detach().numpy()[big_loss])
                    ##print(loss)
                    #print("prediction:")
                    #print(P.cpu().detach().numpy()[big_loss])
                    ##print(P)
                    #print("True value:")
                    #print(T[big_loss])
                    #print("Mask")
                    #print(M[big_loss])
                    #print(T[big_loss][M[big_loss]])
            #exit()



        if self.options.combine_pair_loss.lower() == "mean":
            combined_losses = losses.mean(0)
            index = 0

        if self.options.combine_pair_loss.lower() == "softmin":
            weights = F.softmin(losses, 0)
            combined_losses = (weights * losses).sum(0)

        return combined_losses, index

    def symmetric_divergence(self, predictions: Tuple[Tensor, ...], masks: Tensor) -> Tensor:
        # Make sure the gradient doesnt go through the target variable, only the prediction variable
        # Very unstable to take the gradient through both the target and prediction.
        # Since transpositions are symmetric, this will still be a symmetric operation.
        exp_predictions = [torch.exp(prediction.detach()) for prediction in predictions]

        results = []

        for i, j in self.training_dataset.unordered_event_transpositions:
            div = jensen_shannon_divergence(exp_predictions[i], predictions[i], exp_predictions[j], predictions[j])
            results.append(div * masks[i] * masks[j])

        return sum(results) / len(self.training_dataset.unordered_event_transpositions)

    def training_step(self, batch: Tuple[Tuple[Tensor, Tensor], ...], batch_nb: int) -> Dict[str, Tensor]:
        (source_data, source_mask), *targets = batch



        # ===================================================================================================
        # Network Forward Pass
        # ---------------------------------------------------------------------------------------------------
        predictions = self.forward(source_data, source_mask, debug=False)

        # Extract individual prediction data
        classifications = tuple(prediction[1] for prediction in predictions)
        predictions = tuple(prediction[0] for prediction in predictions)

        # ===================================================================================================
        # Initial log-likelihood loss for classification task
        # ---------------------------------------------------------------------------------------------------
        total_loss, best_indices = self.negative_log_likelihood(predictions, classifications, targets, debug=False)

        # Log the classification loss to tensorboard.
        with torch.no_grad():
            self.log("loss/nll_loss", total_loss.mean())
            if torch.isnan(total_loss).any():
                # For debug reasons
                # total_loss, best_indices = self.negative_log_likelihood(predictions, classifications, targets, debug=True)
                raise ValueError("NLL Loss has diverged.")
            # For debug reasons
           # elif total_loss.mean() > 1e3:
           #     predictions = self.forward(source_data, source_mask, debug=True)
           #     classifications = tuple(prediction[1] for prediction in predictions)
           #     predictions = tuple(prediction[0] for prediction in predictions)
           #     total_loss, best_indices = self.negative_log_likelihood(predictions, classifications, targets, debug=True)
           #     #print(source_data)
           #     #print(source_mask)
           #     #np_source_data = source_data.detach().cpu().numpy()
           #     #np_source_mask = source_mask.detach().cpu().numpy()
           #     #for i, obj in enumerate(np_source_data):
           #     #    print(obj)
           #     #    print(obj.shape)
           #     #    print(np_source_mask[i])
           #     #    print(np_source_mask[i].shape)
           #     #    print(obj[np_source_mask[i]])
           #     #    break
           #     raise ValueError("Loss is greater than 1000, does not make a lot sense!")

        # Construct the newly permuted masks based on the minimal permutation found during NLL loss.
        permutations = self.event_permutation_tensor[best_indices].T
        masks = torch.stack([target[1] for target in targets])
        masks = torch.gather(masks, 0, permutations)

        # ===================================================================================================
        # Auxiliary loss term to prevent distributions from collapsing into single output.
        # ---------------------------------------------------------------------------------------------------
        if self.options.kl_loss_scale > 0:
            # Compute the symmetric loss between all valid pairs of distributions.
            kl_loss = self.symmetric_divergence(predictions, masks)

            with torch.no_grad():
                self.log("loss/symmetric_loss", -kl_loss.mean())
                if torch.isnan(kl_loss).any():
                    raise ValueError("Symmetric KL Loss has diverged.")

            total_loss = total_loss - kl_loss * self.options.kl_loss_scale

        # ===================================================================================================
        # Balance the loss based on the distribution of various classes in the dataset.
        # ---------------------------------------------------------------------------------------------------

        # Balance based on the particles present - only used in partial event training
        if self.balance_particles:
            class_indices = (masks * self.particle_index_tensor.unsqueeze(1)).sum(0)
            class_weights = self.particle_weights_tensor[class_indices]
            total_loss = total_loss * class_weights

        # Balance based on the number of jets in this event
        if self.balance_jets:
            class_indices = source_mask.sum(1)
            class_weights = self.jet_weights_tensor[class_indices]
            total_loss = total_loss * class_weights

        # ===================================================================================================
        # Combine and return the loss
        # ---------------------------------------------------------------------------------------------------

        # TODO Simple mean for speed
        total_loss = len(targets) * total_loss.sum() / masks.sum()
        # total_loss = total_loss.mean()

        self.log("loss/total_loss", total_loss)
        self.accuracy_purity_training(source_data, source_mask, targets)
        self.running_loss.append(total_loss)
        return total_loss

    def accuracy_purity_training(self, source_data, source_mask, targets) -> None:
        jet_predictions, particle_scores = self.predict_jets_and_particle_scores(source_data, source_mask)

        batch_size = source_data.shape[0]
        num_targets = len(targets)

        # Stack all of the targets into single array, we will also move to numpy for easier the numba computations.
        stacked_targets = np.zeros(num_targets, dtype=object)
        stacked_masks = np.zeros((num_targets, batch_size), dtype=np.bool)
        for i, (target, mask) in enumerate(targets):
            stacked_targets[i] = target.detach().cpu().numpy()
            stacked_masks[i] = mask.detach().cpu().numpy()

        metrics = self.evaluator.full_report_string(jet_predictions, stacked_targets, stacked_masks, prefix="Training_Purity/")

        # Apply permutation groups for each target
        for target, prediction, decoder in zip(stacked_targets, jet_predictions, self.decoders):
            for indices in decoder.permutation_indices:
                if len(indices) > 1:
                    prediction[:, indices] = np.sort(prediction[:, indices])
                    target[:, indices] = np.sort(target[:, indices])

        metrics.update(self.compute_metrics(jet_predictions, particle_scores, stacked_targets, stacked_masks))
        
        if self.metrics_training is None:
            self.metrics_training = {}
            for key, value in metrics.items():
                try:
                    if not np.isnan(value):
                       self.metrics_training.update({key: []})
                except TypeError:
                    if not np.isnan(value.cpu().detach().numpy()):
                       self.metrics_training.update({key: []})

        for key in self.metrics_training.keys():
            self.metrics_training[key].append(metrics[key])

    def training_epoch_end(self, output):
        for key, values in self.metrics_training.items():
            self.logger.experiment.add_scalar(key, sum(values)/len(values), self.current_epoch)
            self.metrics_training[key] = []

        self.logger.experiment.add_scalar("epoch/loss", sum(self.running_loss)/len(self.running_loss), self.current_epoch)
        self.running_loss = []

