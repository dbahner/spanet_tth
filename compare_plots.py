from argparse import ArgumentParser
import numpy as np
import matplotlib as mtp
from matplotlib import pyplot as plt

import PyATLASstyle as atlas
def main(compare_files: str,
         working_point: int):
    efficiencies = []
    labels = []
    with open(compare_files, "r") as f:
        for row in f:
            dic, label = row.split(" ")
            efficiencies.append(np.load(dic + f"/plots_{working_point}WP/efficiencies_{working_point}WP.npy", allow_pickle=True)[()])
            labels.append(label.rstrip("\n"))

    atlas.applyATLASstyle(mtp)
    for key in efficiencies[0]:
        fig, ax = plt.subplots(figsize=(7.2, 4.8))
        for i, dics in enumerate(efficiencies):
            dic = dics[key]
            keys = dic.keys()
            vals = np.flip(np.vstack([dic[okey] for okey in keys]), axis=0)
            keys = [r"$" + okey.replace("h", r"_{\mathrm{had}}").replace("l", r"_{\mathrm{lep}}").replace(" ", "+") + r"$" for okey in keys]
            keys.reverse()
            ax.errorbar(keys, vals[:, 0], yerr=vals[:, 1], marker='x', fmt=" ", label=labels[i])
        if key == "reco":
            ax.set_ylabel(r"$\varepsilon_{\mathrm{reco}}$")
        elif key == "matching_fraction":
            ax.set_ylabel(r"$\varepsilon_{\mathrm{match}}$")
        elif key == "purity":
            ax.set_ylabel("Purity")
        ax.set_ylim(0.0, 1)
        ax.set_yticks(np.linspace(0, 1, 11))
        ax.minorticks_on()
        ax.tick_params(axis='x', which='minor', bottom=False)
        ax.grid(b=True, which='both', axis='y')
        ax.grid(b=True, which='major', axis='x', linestyle='--')
        atlas.makeSimulationtag(ax, fig, working_point, xmin=0.6)
        ax.legend(loc='upper left')
        fig.savefig(f"comparisons/compare_{key}_{working_point}.pdf")
        plt.clf()



if __name__=="__main__":
    parser = ArgumentParser()
    parser.add_argument("-cf", "--compare_files", type=str, help="Text file with trainings that should be compared.")
    parser.add_argument("-wp", "--working_point", choices=[60, 70, 77, 85], type=int, help="Working point that should be compared.")
    arguments = parser.parse_args()
    main(**arguments.__dict__)
